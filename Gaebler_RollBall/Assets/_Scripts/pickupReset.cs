﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickupReset : MonoBehaviour
{ 
    //resetTime is how long the object takes to reset
    float resetTime=2f;
    //timer is how long is left on reset clock
    float timer = 0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    
    // Update is called once per frame
    void Update()
    {
        if (timer > 0f)
        {
            //subtract from timer based on time from last update
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                //change back to the child pickup
                Reset();
            }
        }
        
    }

    
 
    public void BeginResetClock()
    {   
   
        print("Clock Start!");
        timer = resetTime;
    }
    
    private void Reset()
    {
        //reactivate the child to make it appear again
        transform.GetChild(0).gameObject.SetActive(true);
    }
    
}



